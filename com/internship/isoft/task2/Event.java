package com.internship.isoft.task2;

import java.util.*;

public abstract class Event
{
    private static Map<String, Calendar> events = new HashMap<String, Calendar>()
    {{
        put("Apple Worldwide Developer Conference", new GregorianCalendar(2019, 10, 20));
        put("Micro Conference", new GregorianCalendar(2019, 10, 26));
        put("Python Conference", new GregorianCalendar(2033, 10, 20));
        put("3rd World Drug Delivery", new GregorianCalendar(2000, 23, 24));
        put("th EACR Conference on Cancer Genomics", new GregorianCalendar(2000, 01, 01));
        put("International Conference on Biomedical and Clinical Research", new GregorianCalendar(2000, 07, 04));
    }};

    public static void findEvent(int month)
    {
        boolean isContains = false;
        for (String event : events.keySet())
        {
            if (events.get(event).get(Calendar.MONTH) == month)
            {
                System.out.println(event);
                isContains = true;
            }
        }
        if (!isContains)
        {
            findEventError();
        }
    }

    public static void findEvent(int day, int month, int year)
    {
        boolean isContains = false;
        for (String event : events.keySet())
        {
            if (events.get(event).get(Calendar.MONTH) == month && events.get(event).get(Calendar.DAY_OF_MONTH) == day &&
            events.get(event).get(Calendar.YEAR) == year)
            {
                isContains = true;
            }
        }
        if (!isContains)
        {
            findEventError();
        }
    }

    public static void findEvent(String eventName)
    {
        if (events.containsKey(eventName))
        {
            System.out.println("The date of event is: " + events.get(eventName).get(Calendar.DAY_OF_MONTH) + "." +
                    events.get(eventName).get(Calendar.MONTH) + "." + events.get(eventName).get(Calendar.YEAR) + "\n");
        }
        else
        {
            findEventError();
        }
    }

    private static void findEventError()
    {
        System.out.println("ERROR: Can't find event/evets in our list with events! \n");
    }

}
