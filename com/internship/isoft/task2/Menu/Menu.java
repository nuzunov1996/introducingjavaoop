package com.internship.isoft.task2.Menu;

import com.internship.isoft.task2.Event;
import com.internship.isoft.task2.Validator;

import javax.swing.*;
import java.util.InputMismatchException;
import java.util.NoSuchElementException;
import java.util.Scanner;

/**
 * User: Nikolay Uzunov
 */
public class Menu
{
    public static void run()
    {
        try
        {
            Scanner input = new Scanner(System.in);

            System.out.println("Menu");
            System.out.println("Choose a car: ");
            System.out.println("1) Get date of event by event's name");
            System.out.println("2) View all events by day of month and month");
            System.out.println("3) View all events by month");
            System.out.println("4) EXIT");

            System.out.print("Choose a option: ");
            int choice = input.nextInt();
            switch (choice)
            {
                case 1:
                {
                    System.out.print("Enter a event's name: ");
                    input.nextLine();
                    String eventName = input.nextLine();
                    Event.findEvent(eventName);
                    run();
                }
                break;

                case 2:
                {
                    System.out.println("Enter a day of month(1-30/31): ");
                    int day = input.nextInt();
                    System.out.println("Enter a month(1-12): ");
                    int month = input.nextInt();
                    System.out.println("Enter a year: ");
                    int year = input.nextInt();
                    if (Validator.areDaysOfMonthCorrect(day, month, year) && Validator.areMonthCorrect(month))
                    {
                        Event.findEvent(day, month, year);
                        run();
                    }
                    else
                    {
                        System.out.println("Wrong entered month or day of month");
                        run();
                    }
                }
                break;

                case 3:
                {
                    System.out.println("Enter a month(1-12): ");
                    int month = input.nextInt();
                    if (Validator.areMonthCorrect(month))
                    {
                        Event.findEvent(month);
                        run();
                    }
                }
                break;
                case 4:
                {
                    return;
                }
                default:
                {
                    System.out.println("Wrong entered number!");
                    run();
                }
            }
        }
        catch (NoSuchElementException e)
        {
            System.out.println("Wrong entered data");
            run();
        }
    }
}
