package com.internship.isoft.task3;

import com.internship.isoft.task3.abstraction.ICalculator;

/**
 * User: Nikolay Uzunov
 */
public abstract class RealNumbersCalculator implements ICalculator<Integer>
{
    @Override
    public Integer substraction(Integer a, Integer b)
    {
        return a - b;
    }

    @Override
    public Integer addtioion(Integer a, Integer b)
    {
        return a + b;
    }

    @Override
    public Integer multiplication(Integer a, Integer b)
    {
        return a * b;
    }

    @Override
    public Integer division(Integer a, Integer b)
    {
        return a / b;
    }
}
