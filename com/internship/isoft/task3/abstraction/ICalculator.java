package com.internship.isoft.task3.abstraction;

/**
 * User: Nikolay Uzunov
 */
public interface ICalculator<T>
{
    T substraction(T a, T b);
    T addtioion(T a, T b);
    T multiplication(T a, T b);
    T division(T a, T b);
}
