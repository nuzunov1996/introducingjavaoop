package com.internship.isoft.task3;

/**
 * User: Nikolay Uzunov
 */

public class ScientificCalculator extends Calculator
{
    public double sin(double x) {
        return Math.sin(Math.toRadians(x));
    }

    public double cos(double x) {
        return Math.cos(Math.toRadians(x));
    }

    public double tan(double x) {
        if (Math.cos(Math.toRadians(x)) != 0)
        {
            return Math.tan(Math.toRadians(x));
        }
        else
        {
            return  0;
        }
    }
}
