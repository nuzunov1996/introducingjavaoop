package com.internship.isoft.task3;

/**
 * User: Nikolay Uzunov
 */
public class ProgrammerCalculator extends RealNumbersCalculator
{
    public String toHex(int number)
    {
        return Integer.toHexString(number).toUpperCase();
    }

    public String toOct(int number)
    {
        return Integer.toOctalString(number);
    }

    public String toBinary(int number)
    {
        return Integer.toBinaryString(number);
    }
}
