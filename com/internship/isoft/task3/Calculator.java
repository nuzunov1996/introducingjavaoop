package com.internship.isoft.task3;

import com.internship.isoft.task3.abstraction.ICalculator;

public class Calculator implements ICalculator<Double>
{

    @Override
    public Double substraction(Double a, Double b)
    {
        return a - b;
    }

    @Override
    public Double addtioion(Double a, Double b)
    {
        return a + b;
    }

    @Override
    public Double multiplication(Double a, Double b)
    {
        return a * b;
    }

    @Override
    public Double division(Double a, Double b)
    {
        return a / b;
    }
}