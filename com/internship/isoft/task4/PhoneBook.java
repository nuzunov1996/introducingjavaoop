package com.internship.isoft.task4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

/**
 * User: Nikolay Uzunov
 */
public class PhoneBook
{
    private Map<String, Contact> contacts = new HashMap<>();

    public Map<String, Contact> getContacts()
    {
        return contacts;
    }

    public void setContacts(Map<String, Contact> contacts)
    {
        this.contacts = contacts;
    }
}
