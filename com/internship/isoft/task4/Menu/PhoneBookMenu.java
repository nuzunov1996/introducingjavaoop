package com.internship.isoft.task4.menu;

import com.internship.isoft.Validator.Validator;
import com.internship.isoft.task4.Contact;
import com.internship.isoft.task4.service.implementation.PhoneBookService;

import java.util.*;

/**
 * User: Nikolay Uzunov
 */
public class PhoneBookMenu
{
    private static PhoneBookService phoneBookService = new PhoneBookService();

    public static void run()
    {
        try
        {
            Scanner input = new Scanner(System.in);

            System.out.println("menu");
            System.out.println("Choose phone book operation: ");
            System.out.println("1) Add new contact");
            System.out.println("2) Search contact");
            System.out.println("3) See all contacts");
            System.out.println("4) Delete contact");
            System.out.println("5) EXIT");

            System.out.print("Choose a option: ");
            int choice = Integer.valueOf(input.nextLine());
            switch (choice)
            {
                case 1:
                {
                    insertContactMenu(input);
                    run();
                }
                break;
                case 2:
                {
                    searchMenu(input);
                    run();
                }
                break;
                case 3:
                {
                    showAllContactsMenu();
                    run();
                }
                break;
                case 4:
                {
                    deleteContactsMenu(input);
                    run();
                }
                case 5:
                {
                    return;
                }
                default:
                {
                    System.out.println("Wrong entered number!");
                    run();
                }
            }
        }
        catch (NumberFormatException e)
        {
            System.out.println("Wrong entered data");
            run();
        }
    }

    private static void insertContactMenu(Scanner input)
    {
        System.out.println("Enter contact's name: ");
        String name = input.nextLine();
        System.out.println("Enter contact's phone number: ");
        String phoneNumber = input.nextLine();
        Contact contact = new Contact(name, phoneNumber);

        if (Validator.isPhoneNumberValid(phoneNumber))
        {
            phoneBookService.insertContact(contact);
            System.out.println("Adding is Succesfull!");
        }
        else
        {
            System.out.println("The phone number is invalid!");
            run();
        }
    }

    private static void searchMenu(Scanner input)
    {
        System.out.println("menu");
        System.out.println("Choose kind of search: ");
        System.out.println("1) Search by phone number");
        System.out.println("2) Search by name");
        System.out.println("3) Back to Main menu");
        System.out.println("4) EXIT");

        System.out.print("Enter your choice: ");
        int choice = Integer.valueOf(input.nextLine());
        switch (choice)
        {
            case 1:
            {
                try
                {
                    searchByPhoneNumberMenu(input);
                }
                catch (NullPointerException e)
                {
                    System.out.println("The phone number doesn't contains in this phone book!");
                    searchMenu(input);
                }
            }
            break;
            case 2:
            {
                try
                {
                    searchByNameMenu(input);
                }
                catch (NullPointerException e)
                {
                    System.out.println("The phone number doesn't contains in this phone book!");
                    searchMenu(input);
                }
            }
            break;
            case 3:
            {
                run();
            }
            case 4:
            {
                return;
            }
            default:
            {
                System.out.println("Wrong entered number!");
                run();
            }
        }

    }

    private static void searchByPhoneNumberMenu(Scanner input)
    {
        System.out.println("Enter contact's phone number: ");
        String phoneNumber = input.nextLine();
        if (Validator.isPhoneNumberValid(phoneNumber))
        {
            Contact contact = phoneBookService.getContactByPhoneNumber(phoneNumber);
            System.out.println(contact.toString());
        }
        else
        {
            System.out.println("The phone number is invalid!");
            searchMenu(input);
        }

    }

    private static void searchByNameMenu(Scanner input)
    {
        System.out.println("Enter contact's name: ");
        String name = input.nextLine();

        ArrayList<Contact> contacts = (ArrayList<Contact>) phoneBookService.getContactsByName(name);
        if (contacts.isEmpty())
        {
            System.out.println("Тhere are no contacts with this name!");
            searchMenu(input);
        }
        else
        {
            for (Contact contact : contacts)
            {
                System.out.println(contact.toString());
            }
        }

    }

    private static void showAllContactsMenu()
    {
        ArrayList<Contact> contacts = (ArrayList<Contact>) phoneBookService.getContacts();
        if (contacts.isEmpty())
        {
            System.out.println("The phone book is empty");
        }
        else
        {
            for (Contact contact : contacts)
            {
                System.out.println(contact.toString());
            }
        }

    }

    private static void deleteContactsMenu(Scanner input)
    {
        System.out.println("Enter contact's phone number: ");
        String phoneNumber = input.nextLine();
        if (Validator.isPhoneNumberValid(phoneNumber))
        {
            phoneBookService.deleteContact(phoneNumber);
        }
        else
        {
            System.out.println("The phone number is invalid!");
            run();
        }
    }
}
