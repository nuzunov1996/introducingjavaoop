package com.internship.isoft.task4.service.abstraction;

import com.internship.isoft.task4.Contact;

import java.util.Collection;

/**
 * User: Nikolay Uzunov
 */
public interface Service<T>
{
   T getContactByPhoneNumber(String phoneNumber);
   Collection<T> getContactsByName(String contactName);
   Collection<T> getContacts();
   void insertContact(T contact);
   void deleteContact(String phoneNumber);
}
