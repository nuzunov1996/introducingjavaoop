package com.internship.isoft.task4.service.implementation;

import com.internship.isoft.task4.Contact;
import com.internship.isoft.task4.PhoneBook;
import com.internship.isoft.task4.service.abstraction.Service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;

/**
 * User: Nikolay Uzunov
 */
public class PhoneBookService implements Service<Contact>
{
    private PhoneBook phoneBook = new PhoneBook();
    private Map<String, Contact> contacts = phoneBook.getContacts();

    @Override
    public Contact getContactByPhoneNumber(String phoneNumber)
    {
        return phoneBook.getContacts().get(phoneNumber);
    }

    @Override
    public Collection<Contact> getContactsByName(String contactName)
    {
        Collection<Contact> contactsList = new ArrayList<>();
        for (Contact contact : contacts.values())
        {
            if (contact.getName().equals(contactName))
            {
                contactsList.add(contact);
            }
        }
        return contactsList;
    }

    @Override
    public Collection<Contact> getContacts()
    {
        Collection<Contact> contactsList = new ArrayList<>();

        for (Contact contact: contacts.values())
        {
            contactsList.add(contact);
        }
        return contactsList;
    }

    @Override
    public void insertContact(Contact contact)
    {
        if (isPhoneNumberExist(contact.getPhoneNumber()))
        {
            System.out.println("The contact already exist in this phone book!");
        }
        else
        {
            contacts.put(contact.getPhoneNumber(), contact);
        }
    }

    @Override
    public void deleteContact(String phoneNumber)
    {
        if (isPhoneNumberExist(phoneNumber))
        {
            contacts.remove(phoneNumber);
            System.out.println("Deleting is Successful!");
        }
        else
        {
            System.out.println("Contact with this number doesn't exist!");
        }
    }

    private boolean isPhoneNumberExist(String phoneNumber)
    {
        if (contacts.containsKey(phoneNumber))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
}
