package com.internship.isoft.task4;

import java.io.Serializable;

/**
 * User: Nikolay Uzunov
 */
public class Contact implements Serializable
{
    private String name;
    private String phoneNumber;

    public Contact(String name, String phoneNumber)
    {
        this.name = name;
        this.phoneNumber = phoneNumber;
    }

    public String getName()
    {
        return this.name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getPhoneNumber()
    {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber)
    {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public String toString()
    {
        return "Contact name: " + this.name + ", Contact phone number: " + this.phoneNumber;
    }

}
