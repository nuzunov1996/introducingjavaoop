package com.internship.isoft.task4;

import com.internship.isoft.task4.menu.PhoneBookMenu;

/**
 * User: Nikolay Uzunov
 */
public class Main
{
    public static void main(String[] args)
    {

        PhoneBookMenu.run();

    }
}
