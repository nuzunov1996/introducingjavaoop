package com.internship.isoft.task1.Menu;

import com.internship.isoft.task1.Cars.*;
import com.internship.isoft.task1.Engines.AudiEngine;
import com.internship.isoft.task1.Engines.BmwEngine;
import com.internship.isoft.task1.Engines.VolkswagenEngine;
import com.internship.isoft.task1.Main;

import java.util.ArrayList;
import java.util.Scanner;

public class Menu
{
    private static Car hondaCar = new Honda("red", "civic");
    private static Car bmwCar = new Bmw("black", "e90", new BmwEngine(306, 3500, "pretrol"));
    private static Car audiCar = new Audi("yellow", "A3", new AudiEngine(306, 3500, "pretrol"));
    private static Car volkswagenCar = new Volkswagen("green", "Golf4", new VolkswagenEngine(306, 3500, "pretrol"));

    public static void run()
    {
        try (Scanner input = new Scanner(System.in))
        {
            System.out.println("Menu");
            System.out.println("Choose a car: ");
            System.out.println("1) Brand: " + hondaCar.getBrand() + " | Model: " + hondaCar.getModel());
            System.out.println("2) Brand: " + bmwCar.getBrand() + " | Model: " + bmwCar.getModel());
            System.out.println("3) Brand: " + audiCar.getBrand() + " | Model: " + audiCar.getModel());
            System.out.println("4) Brand: " + volkswagenCar.getBrand() + " | Model: " + volkswagenCar.getModel());
            System.out.println("5) EXIT \n");

            int choice = input.nextInt();
            switch (choice)
            {
                case 1:
                {
                    innerMenu(hondaCar);
                }
                break;

                case 2:
                {
                    innerMenu(bmwCar);
                }
                break;

                case 3:
                {
                    innerMenu(audiCar);
                }
                break;

                case 4:
                {
                    innerMenu(volkswagenCar);
                }
                break;
                case 5:
                {
                    return;
                }
                default:
                {
                    System.out.println("Wrong entered number!");
                    run();
                }

            }
        }
        catch (Exception e)
        {
            System.out.println("Wrong entered data");
            Main.main(null);
        }
    }

    private static void innerMenu(Car car)
    {
        Scanner input = new Scanner(System.in);
        System.out.println("Menu");
        System.out.println("1) Start engine.");
        System.out.println("2) Drive the car.");
        System.out.println("3) Info");
        System.out.println("4) Return back to main menu");
        System.out.println("5) EXIT \n");
        System.out.println("Choose a option: ");
        int choice = input.nextInt();

        switch (choice)
        {
            case 1:
            {
                car.startCar();
                innerMenu(car);
            }
            break;
            case 2:
            {
                car.drive();
                innerMenu(car);
            }
            break;
            case 3:
            {
                car.displayCharaceristic();
                innerMenu(car);
            }
            break;
            case 4:
            {
                car.displayCharaceristic();
                Main.main(null);

            }
            break;
            case 5:
            {
                return;
            }
            default:
            {
                System.out.println("Wrong entered number!");
                innerMenu(car);
            }
        }
    }
}
