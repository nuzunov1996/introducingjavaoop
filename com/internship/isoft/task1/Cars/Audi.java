package com.internship.isoft.task1.Cars;

import com.internship.isoft.task1.Engines.AudiEngine;

public class Audi extends Car{

    public Audi(String color, String model)
    {
        super("Audi", color, model, new AudiEngine());
    }

    public Audi( String color, String model, AudiEngine engine)
    {
        super("Audi", color, model, engine);
    }

    @Override
    public void startCar()
    {
        System.out.println("Whir \n");
    }
}
