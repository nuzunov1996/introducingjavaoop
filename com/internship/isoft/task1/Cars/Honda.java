package com.internship.isoft.task1.Cars;

import com.internship.isoft.task1.Engines.HondaEngine;

public class Honda extends Car {

    public Honda(String color, String model)
    {
        super("Honda", color, model, new HondaEngine());
    }
    public Honda(String color, String model, HondaEngine engine)
    {
        super("Honda", color, model, engine);
    }

    @Override
    public void startCar()
    {
        System.out.println("Whir \n");
    }
}
