package com.internship.isoft.task1.Cars;

import com.internship.isoft.task1.Engines.BmwEngine;
import com.internship.isoft.task1.Engines.VolkswagenEngine;

public class Volkswagen extends  Car{

    public Volkswagen(String color, String model)
    {
        super("Volkswagen", color, model, new BmwEngine());
    }

    public Volkswagen(String color, String model, VolkswagenEngine engine)
    {
        super("Volkswagen", color, model, engine);
    }

    @Override
    public void startCar()
    {
        System.out.println("Whir \n");
    }
}
