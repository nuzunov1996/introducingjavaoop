package com.internship.isoft.task1.Cars;

import com.internship.isoft.task1.Engines.BmwEngine;

public class Bmw extends Car {

    public Bmw(String color, String model)
    {
        super("Bmw", color, model, new BmwEngine());
    }

    public Bmw(String color, String model, BmwEngine engine)
    {
        super("Bmw", color, model, engine);
    }

    @Override
    public void startCar()
    {
        System.out.println("Roar \n");
    }
}
