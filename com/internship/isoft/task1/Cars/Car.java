package com.internship.isoft.task1.Cars;

import com.internship.isoft.task1.Engines.Engine;

public abstract class Car {
   private String brand;
   private String model;
   private String color;
   private Engine engine;

    protected Car(String brand, String color, String model, Engine engine)
    {
        setBrand(brand);
        setColor(color);
        setModel(model);
        setEngine(engine);
    }

    public String getBrand()
    {
        return brand;
    }

    public void setBrand(String brand)
    {
        this.brand = brand;
    }

    public String getModel()
    {
        return model;
    }

    public void setModel(String model)
    {
        this.model = model;
    }

    public String getColor()
    {
        return color;
    }

    public void setColor(String color)
    {
        this.color = color;
    }

    public Engine getEngine()
    {
        return engine;
    }

    public void setEngine(Engine engine)
    {
        this.engine = engine;
    }

    public void drive()

    {
        System.out.println("Driving... \n");
    }

    public void displayCharaceristic()
    {
        System.out.println("Brand: " + getBrand()+ "\n" + "Model: " + getModel()+ "\n" + "Color: " + getColor()+ "\n"
            + "Engine: " + getEngine().displayCharacteristics() + "\n");
    }

    public abstract void startCar();
}
