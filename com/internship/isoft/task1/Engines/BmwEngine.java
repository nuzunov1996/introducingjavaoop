package com.internship.isoft.task1.Engines;

public class BmwEngine extends Engine  {
    private static String name = "Bmw engine";

    public BmwEngine()
    {
        super();
    }

    public BmwEngine(int horsePower, int cubicMetre, String engineType)
    {
        super(horsePower, cubicMetre, engineType);
    }

    public static String getName() {
        return name;
    }

    @Override
    public String displayCharacteristics() {
        String engineCharacteristics = "\nEngine name: " + name + "\n" + "Horse power: " + getHorsePower()+ "\n" +
                "Cubic metre: " + getCubicMetre()+ "\n" + "Engine type: " + getEngineType()+ "\n";

        return engineCharacteristics;
    }
}
