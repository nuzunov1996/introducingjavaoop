package com.internship.isoft.task1.Engines;

public abstract class Engine {
    private int horsePower;
    private int cubicMetre;
    private String engineType;


    protected Engine() {
        setCubicMetre(1900);
        setEngineType("diesel");
        setHorsePower(150);
    }

    protected Engine(int horsePower, int cubicMetre, String engineType) {
        setHorsePower(horsePower);
        setCubicMetre(cubicMetre);
        setEngineType(engineType);
    }

    public int getHorsePower() {
        return horsePower;
    }

    public void setHorsePower(int horsePower) {
        this.horsePower = horsePower;
    }

    public int getCubicMetre() {
        return cubicMetre;
    }

    public void setCubicMetre(int cubicMetre) {
        this.cubicMetre = cubicMetre;
    }

    public String getEngineType() {
        return engineType;
    }

    public void setEngineType(String engineType) {
        this.engineType = engineType;
    }

    public abstract String displayCharacteristics();
}
