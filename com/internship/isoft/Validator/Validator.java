package com.internship.isoft.Validator;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.regex.Pattern;

/**
 * User: Nikolay Uzunov
 */
public class Validator
{
    public static boolean areDaysOfMonthCorrect(int day, int month, int year)
    {
        Calendar calendar = new GregorianCalendar(year, month, day);
        int numberOfDaysOfMonth = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
        if (day <= numberOfDaysOfMonth && day > 0)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static boolean areMonthCorrect(int month)
    {
        if (month > 0 && month <= 12)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    public static boolean isPhoneNumberValid(String phoneNumber) {
        String regex = "^(08[789][0-9]{7})$";
        return Pattern.compile(regex).matcher(phoneNumber).matches();
    }
}
